# Quick Demo of a few things

1. Pod with 1 container running containeous/whoami
2. Route with generic app/service/namespace hostname (via wildcard cert)
3. Route with custom hostname (needs aka work done first)
    [OKD Route w/ Hostname reference](https://docs.okd.io/3.11/architecture/networking/routes.html#route-hostnames)
4. openshift-acme deployment to maintain #3
    [oit-automation/helm/openshift-acme chart](https://git-internal.oit.duke.edu/oit-automation/helm/openshift-acme)
    [tnozicka/openshift-acme](https://github.com/tnozicka/openshift-acme)

example deployment with helm:

```bash
helm install whoami chart/ --set cloudHostname=whoami-project-name.cloud.duke.edu,nonCloudHostname=something.somewhere.duke.edu

helm repo add automation-charts https://automation-charts.cloud.duke.edu
helm install openshift-acme automation-charts/openshift-acme --set acmeContactEmail=email.address@duke.edu

# teardown:
helm uninstall openshift-acme
helm uninstall whoami
```

extra credit, use helmfile!

```bash
# edit helmfile.yaml and put in your stuff
helmfile apply
# YO, helmfile is THE BUSINESS!

# teardown:
helmfile destroy
```
